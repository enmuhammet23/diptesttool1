
from flask import Flask, json, g, request, jsonify, json
from random import choice

app = Flask(__name__)



@app.route("/evaluate", methods=["POST"])
def evaluate():
    json_data = json.loads(request.data)
    givenSentence = str(json_data['textarea']).split()
    givenSentence.remove(choice(givenSentence))
    result = {"text": " ".join(givenSentence)}
    response = app.response_class(
        response=json.dumps(result),
        status=200,
        mimetype='application/json'
    )
    return response

if __name__ == "__main__":
   app.run(host='0.0.0.0')


